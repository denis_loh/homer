export function omit<T extends object, K extends keyof T>(target: T, ...omitKeys: K[]): Omit<T, K> {
  return (Object.keys(target) as K[]).reduce(
    (res, key) => {
      if (!omitKeys.includes(key)) {
        res[key] = target[key];
      }
      return res;
    },
    {} as any
  );
}

export function removeLastDirectoryFromPath(path: string) : string {
  let the_arr = path.split('/');
  the_arr.pop();
  return( the_arr.join('/') );
}

export function getPathDepth(path: string) : number {
  return path.split('/').length - 1;
}
