import * as React from 'react';
import {
  Theme,
  Menu,
  MenuItem,
  Typography,
  AppBar,
  Toolbar as MaterialToolbar,
  IconButton,
  createStyles,
  WithStyles,
  withStyles,
  Icon,
  Badge,
  Zoom
} from '@material-ui/core';
import {
  MoreVert as MoreIcon
} from '@material-ui/icons';
import { MenuItem as MenuItemModel } from '../'
import { Link } from 'react-router-dom' 
import { Hamburger } from '../Hamburger';

const styles = ({ breakpoints }: Theme) => createStyles({
  grow: {
    flexGrow: 1
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  },
  sectionDesktop: {
    display: 'none',
    [breakpoints.up('sm')]: {
      display: 'flex'
    }
  },
  sectionMobile: {
    display: 'flex',
    [breakpoints.up('sm')]: {
      display: 'none'
    }
  }
});

namespace Toolbar {
  interface Props {
    title: string
    drawer: boolean
    onUp?: () => void
    menu?: MenuItemModel[]
  }

  export interface State {
    mobileMoreAnchorEl?: EventTarget & HTMLElement
  }

  export type AllProps = Props & WithStyles<typeof styles>
}

export const Toolbar = withStyles(styles)(
class Toolbar extends React.Component<Toolbar.AllProps, Toolbar.State> {

  constructor(props: Toolbar.AllProps, context?: any) {
    super(props, context);
    this.state = { mobileMoreAnchorEl: undefined };
  }

  private loadActionItems(items?: MenuItemModel[]) : any {
    let allItems: MenuItemModel[];
    let visibleItems: MenuItemModel[];
    let truncatedItems: MenuItemModel[];

    if (items !== undefined) {
      allItems = items
    } else {
      allItems = new Array<MenuItemModel>();
    }

    visibleItems = new Array<MenuItemModel>();
    truncatedItems = new Array<MenuItemModel>();

    allItems.map((item) => {
      if (item.truncate !== true) {
        visibleItems.push(item)
      } else {
        truncatedItems.push(item)
      }
    });

    return {
      allItems: allItems,
      visibleItems: visibleItems,
      truncatedItems: truncatedItems
    }
  }

  private handleMobileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    this.setState({ mobileMoreAnchorEl: event.currentTarget });
  };

  private handleMobileMenuClose = () => {
    this.setState({ mobileMoreAnchorEl: undefined });
  };

  private handleUp = () => () => {
    if (this.props.onUp != undefined)
      this.props.onUp();
  };

  private renderActionItem(item: MenuItemModel, index: number, label?: boolean) {
    const { title, target, key, icon, badge } = item
    return (
      <Zoom in={true} style={{ transitionDelay: `${index * 50}ms` }}>
        <div>
          <IconButton color="inherit" aria-label={title} key={key} component={({ innerRef, ...props }) => <Link {...props} to={target}/>}>
            <Badge color="secondary" badgeContent={badge !== undefined ? badge : 0} invisible={badge === undefined}>
            { icon !== undefined ? <Icon>{icon}</Icon> : title }
            </Badge>
          </IconButton>
          { label === true &&
            <p>{title}</p>
          }
        </div>
      </Zoom>
    )
  }

  private renderMenuItem(item: MenuItemModel) {
    const { title, target, key, icon, badge } = item
    return (
        <MenuItem key={key} component={({ innerRef, ...props }) => <Link {...props} to={target}/>}>
          <IconButton color="inherit" aria-label={title}>
            <Badge color="secondary" badgeContent={badge !== undefined ? badge : 0} invisible={badge === undefined}>
            { icon !== undefined ? <Icon>{icon}</Icon> : title }
            </Badge>
          </IconButton>
          <p>{title}</p>
        </MenuItem>
    )
  }

  private renderMobileMenu(items: MenuItemModel[]) {
    const { mobileMoreAnchorEl } = this.state;
    const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

    return (
      <Menu
        anchorEl={mobileMoreAnchorEl}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={isMobileMenuOpen}
        onClose={this.handleMobileMenuClose}
      >
        {
          items.map((item) => (
            this.renderMenuItem(item)
          ))
        }
      </Menu>
    );
  }

  private renderSectionMobile(items: MenuItemModel[]) {
    return (
      <React.Fragment>
        {
          items.map((item, index) => (
              this.renderActionItem(item, items.length - index)
          ))
        }
        <IconButton aria-haspopup="true" onClick={this.handleMobileMenuOpen} color="inherit" key="more">
          <MoreIcon/>
        </IconButton>
      </React.Fragment>
    );
  }

  // noinspection JSMethodCanBeStatic
  private renderSectionDesktop(items: MenuItemModel[]) {
    return (
      items.map((item, index) => (
        this.renderActionItem(item, items.length - index - 1)
      ))
    );
  }

  private renderHomeButton() {
    const { drawer, onUp, classes } = this.props

    return (
      onUp !== undefined &&            
        <IconButton className={classes.menuButton} color="inherit" aria-label="Menu"
                    onClick={this.handleUp()}>
          { drawer ? <Hamburger /> : <Hamburger active /> }
        </IconButton>
    )
  }

  render() {
    const { classes, title, menu } = this.props;

    const { allItems, visibleItems, truncatedItems } = this.loadActionItems(menu)

    return (
      <React.Fragment>
        <AppBar position="static">
          <MaterialToolbar>
            { this.renderHomeButton() }
            <Typography variant="h6" color="inherit" className={classes.grow}>
              {title}
            </Typography>
            <div className={classes.sectionDesktop}>
              {this.renderSectionDesktop(allItems)}
            </div>
            <div className={classes.sectionMobile}>
              {
                truncatedItems.length > 0 ? this.renderSectionMobile(visibleItems) : this.renderSectionDesktop(allItems)
              }
            </div>
          </MaterialToolbar>
        </AppBar>
        {this.renderMobileMenu(truncatedItems)}
      </React.Fragment>
    );
  }

})
