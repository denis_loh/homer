export * from './Drawer'
export * from './Toolbar'

export function isMenuGroup(menu : MenuGroup | MenuItem): menu is MenuGroup {
  return (menu as MenuGroup).items !== undefined
}

export interface MenuGroup {
  title: string,
  key: string,
  items: MenuItem[]
}

export interface MenuItem {
  title: string,
  key: string,
  target: string,
  icon?: string,
  truncate?: boolean,
  badge?: number
}
