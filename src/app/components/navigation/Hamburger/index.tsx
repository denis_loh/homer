import * as React from 'react'
import {
    WithStyles,
    withStyles,
    createStyles
} from '@material-ui/core'

const hamburgerSize                 = 24;
const hamburgerLayerWidth           = 18;
const arrowLayerWidth               = 16;
const hamburgerLayerHeight          = 2;
const hamburgerLayerSpacing         = 3;
const hamburgerLayerBorderRadius    = 2;

const styles = () => createStyles({
    hamburger: {
        width: hamburgerSize,
        height: hamburgerSize,
        display: 'inline-block',
        cursor: 'pointer',

        transitionProperty: 'opacity, filter',
        transitionDuration: '0.15s',
        transitionTimingDuration: 'linear',

        font: 'inherit',
        color: 'inherit',
        textTransform: 'none',
        backgroundColor: 'transparent',
        border: 0,
        margin: 0,
        overflow: 'visible',

        "&.is-active $hamburgerInner, $hamburgerInner::before, $hamburgerInner::after": {            
            backgroundColor: 'currentColor'
        }
    },

    hamburgerBox: {
        width: hamburgerLayerWidth,
        height: (hamburgerLayerHeight * 3 + hamburgerLayerSpacing * 2),
        display: 'inline-block',
        position: 'relative'
    },

    hamburgerInner: {
        display: 'block',
        top: '50%',
        marginTop: (hamburgerLayerHeight * 2 + hamburgerLayerSpacing * 2 + hamburgerLayerHeight/2) / -2,

        "&, &::before, &::after": {
            width: hamburgerLayerWidth,
            height: hamburgerLayerHeight,
            backgroundColor: 'currentColor',
            borderRadius: hamburgerLayerBorderRadius,
            position: 'absolute',
            transitionProperty: 'transform',
            transitionDuration: '0.15s',
            transitionTimingFunction: 'ease'
        },

        "&::before, &::after" : {
            content: '""',
            display: "block"
        },

        "&::before": {
            top: (hamburgerLayerSpacing + hamburgerLayerHeight) * -1
        },

        "&::after": {
            bottom: (hamburgerLayerSpacing + hamburgerLayerHeight) * -1
        }
    },

    hamburgerArrow: {
        "&.is-active $hamburgerInner": {
            transform: `scale(${arrowLayerWidth / hamburgerLayerWidth}, 1)`,

            "&::before": {
                transform: `translate3d(${-hamburgerLayerWidth / 4}px, ${hamburgerLayerSpacing / 2}px, 0) rotate(-40deg) scale(0.66, 1)`
            },

            "&::after": {
                transform: `translate3d(${-hamburgerLayerWidth / 4}px, ${-hamburgerLayerSpacing / 2}px, 0) rotate(40deg) scale(0.66, 1)`
            }
        }
    }
})

export namespace Hamburger {
    export interface Props {
        active?: boolean
    }

    export type AllProps = Props & WithStyles<typeof styles>
}

export const Hamburger = withStyles(styles)(
    class Hamburger extends React.Component<Hamburger.AllProps> {
        render() {
            const { classes, active } = this.props

            const buttonClasses = [ classes.hamburger, classes.hamburgerArrow ].join(" ")
            const activeButtonClasses = [ classes.hamburger, classes.hamburgerArrow, "is-active" ].join(" ")

            return(
                <div className={active ? activeButtonClasses : buttonClasses}>
                    <div className={classes.hamburgerBox}>
                        <div className={classes.hamburgerInner} />
                    </div>
                </div>
            )
        }
    }
)