import * as React from 'react';
import {
  Icon,
  List,
  ListItem,
  ListSubheader,
  ListItemIcon,
  ListItemText,
  Drawer as MaterialDrawer,
  createStyles,
  withStyles,
  WithStyles
} from '@material-ui/core';
import { Link } from 'react-router-dom';
import { MenuItem, MenuGroup, isMenuGroup } from '../'

const styles = createStyles({
  list: {
    width: 250
  }
});

export namespace Drawer {

  export interface Props {
    menu: (MenuGroup | MenuItem)[]
    isOpen: boolean
    selected: string
    onToggle: (open: boolean) => void
  }

  export type AllProps = Props & WithStyles<typeof styles>
}

export const Drawer = withStyles(styles)(
class Drawer extends React.Component<Drawer.AllProps> {

  toggleDrawer = (open: boolean) => () => {
    this.props.onToggle(open);
  };

  renderMenuItem(item: MenuItem) {
    const { selected } = this.props
    const { key, icon, title, target} = item

    return (
      <ListItem button component={({ innerRef, ...props }) => <Link {...props} to={target}/>} key={key} selected={ key === selected } >
        {icon !== undefined ? <ListItemIcon><Icon>{icon}</Icon></ListItemIcon> : ''}
        <ListItemText primary={title}/>
      </ListItem>
    );
  }

  renderMenu() {
    const { menu, classes } = this.props;

    return (
      <div className={classes.list}>
        {
          menu.map((element) => (
            isMenuGroup(element) ?
                <List
                key={element.key}
                component="nav"
                subheader={<ListSubheader component="div">{element.title}</ListSubheader>}>
                {
                    (element.items as MenuItem[]).map((item) => (
                        this.renderMenuItem(item)
                    ))
                }
                </List>
            :
                this.renderMenuItem(element)
          ))
        }
      </div>
    );
  }

  render() {
    const { isOpen } = this.props;

    return (
      <React.Fragment>
        <MaterialDrawer open={isOpen} onClose={this.toggleDrawer(false)}>
          <div
            tabIndex={0}
            role="button"
            onClick={this.toggleDrawer(false)}
            onKeyDown={this.toggleDrawer(false)}
          >
            {this.renderMenu()}
          </div>
        </MaterialDrawer>
      </React.Fragment>
    );
  }
})
