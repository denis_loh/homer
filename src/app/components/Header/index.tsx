import * as React from 'react';
import {
  Drawer,
  Toolbar,
  MenuItem,
  MenuGroup
} from '../navigation';
import {
  withRouter,
  RouteComponentProps
} from 'react-router';
import { removeLastDirectoryFromPath } from 'app/utils';

export namespace Header {
  export interface Props {
    title: string
    drawerMenu?: (MenuItem | MenuGroup)[]
    actionMenu?: MenuItem[]
  }

  export interface State {
    isDrawerOpen: boolean
    selected: string
  }

  export type AllProps = Props & RouteComponentProps
}

export const Header = withRouter(
class Header extends React.Component<Header.AllProps, Header.State> {
  constructor(props: Header.AllProps, context?: any) {
    super(props, context);
    this.state = { selected: 'overview', isDrawerOpen: false };
    this.onToggle = this.onToggle.bind(this);
    this.onUp = this.onUp.bind(this)
  }

  onToggle(open: boolean) {
    this.setState({ isDrawerOpen: open });
  }

  onUp() {
    if (this.props.drawerMenu !== undefined) {
      this.setState({ isDrawerOpen: true});
    } else {
      this.props.history.push(removeLastDirectoryFromPath(this.props.location.pathname))
    }
  }

  render() {
    const { drawerMenu, actionMenu } = this.props
    const { isDrawerOpen, selected } = this.state

    return (
      <header>
        <Toolbar onUp={this.onUp} {...this.props} menu={actionMenu} drawer={drawerMenu !== undefined} />
        { drawerMenu !== undefined &&
          <Drawer isOpen={isDrawerOpen} onToggle={this.onToggle} menu={drawerMenu} selected={selected} />
        }
      </header>
    );
  }
})
