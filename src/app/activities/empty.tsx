import * as React from 'react'
import {
  Theme,
  createStyles,
  WithStyles,
  withStyles,
  Paper,
  Typography
} from '@material-ui/core'

const emptyStyles = (theme: Theme) => createStyles({
  paper: {
    paddingTop: theme.spacing.unit * 8,
    paddingBottom: theme.spacing.unit * 8
  },
  heading: {
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2
  }
})

export namespace Empty {
  export interface Props {
  }

  export type AllProps = Props & WithStyles<typeof emptyStyles>
}

export const Empty = withStyles(emptyStyles)(
  class Empty extends React.Component<Empty.AllProps> {
    render() {
      const { classes } = this.props
      return (
        <Paper className={classes.paper}>
          <Typography variant="h5" align="center" className={classes.heading}>
            Feature not yet implemented.
          </Typography>
          <Typography component="p" align="center">
            Nothing yet to see here. Come again later!
          </Typography>
        </Paper>
      )
    }
  }
)