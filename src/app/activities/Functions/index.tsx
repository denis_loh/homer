import * as React from 'react'
import { 
    Paper,
    Typography,
    Theme,
    createStyles,
    WithStyles,
    withStyles
} from '@material-ui/core'
import {
    RouteComponentProps,
    withRouter,
    Link
} from 'react-router-dom'

const styles = (theme: Theme) => createStyles({
    paper: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
        marginTop: theme.spacing.unit,
        marginBottom: theme.spacing.unit
    }
});

namespace Functions {
    export interface Props {

    }

    export type AllProps = Props & WithStyles<typeof styles> & RouteComponentProps
}

export const Functions = withStyles(styles)(withRouter(
class Functions extends React.Component<Functions.AllProps> {

    render() {
        const { classes, match } = this.props;

        return (
          <React.Fragment >
            {["Hallo", "Welt", "Rabarberkuchen"].map((value:string) => (
              <Paper className={classes.paper}>
                <Typography component="p">
                  <Link to={`${match.path}/${value}`}>{value}</Link>
                </Typography>
              </Paper>
            ))}
          </React.Fragment>
        )
    }

}
))
