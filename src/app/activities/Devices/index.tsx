import * as React from 'react'
import { 
    Typography, IconButton
} from '@material-ui/core'
import {
    ArrowBack,
    Menu
} from '@material-ui/icons'
import { Hamburger } from 'app/components/navigation/Hamburger';

export class Devices extends React.Component {

    render() {
        return (
            <React.Fragment>
                <Typography component="p">
                    Devices
                </Typography>
                <div>
                    <IconButton>
                        <ArrowBack fontSize="large" />
                    </IconButton>
                    <IconButton>
                        <Menu />
                    </IconButton>
                    <IconButton>
                        <Hamburger />
                    </IconButton>
                    <IconButton>
                        <Hamburger active />
                    </IconButton>
                </div>
            </React.Fragment>
        )
    }

}