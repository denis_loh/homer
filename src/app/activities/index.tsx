import * as React from 'react'
import { 
    createStyles,
    Theme,
    WithStyles,
    withStyles,
    Paper,
    Typography
} from '@material-ui/core';

const styles = (theme: Theme) => createStyles({
    container: {
      ...theme.mixins.gutters(),
      padding: theme.spacing.unit
    }
  });

export namespace Activity {
    export interface Props {
        component : React.ComponentType<any>
    }

    export type AllProps = Props & WithStyles<typeof styles>
}

export const Activity = withStyles(styles)(
class Activity extends React.Component<Activity.AllProps> {

    render() {
        const { component:Component, classes } = this.props

        return (
            <div className={classes.container}>
                <Component />
            </div>
        )
    }

}
)

const emptyStyles = (theme: Theme) => createStyles({
  paper: {
    paddingTop: theme.spacing.unit * 8,
    paddingBottom: theme.spacing.unit * 8
  },
  heading: {
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2
  }
})

export namespace Empty {
  export interface Props {
  }

  export type AllProps = Props & WithStyles<typeof emptyStyles>
}

export const Empty = withStyles(emptyStyles)(
  class Empty extends React.Component<Empty.AllProps> {
    render() {
      const { classes } = this.props
      return (
        <Paper className={classes.paper}>
          <Typography variant="h5" align="center" className={classes.heading}>
            Feature not yet implemented.
          </Typography>
          <Typography component="p" align="center">
            Nothing yet to see here. Come again later!
          </Typography>
        </Paper>
      )
    }
  }
)

export * from './Devices';
export * from './Functions';