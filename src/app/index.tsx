import * as React from 'react';
import { Route, Switch, withRouter, RouteComponentProps } from 'react-router';
import App from 'app/containers/App';
import { hot } from 'react-hot-loader';
import { Header } from 'app/components/Header';
import { 
  Activity,
  Devices,
  Functions,
  Empty
} from 'app/activities';
import { 
  TransitionGroup
} from 'react-transition-group';
import {
  Slide,
  WithStyles,
  createStyles,
  withStyles
} from '@material-ui/core';
import { getPathDepth } from './utils';

const ActivityRoute = (props: any) => {
  const { activity, ...newProps } = props;
  return (
    <Route {...newProps} render={() => <Activity component={activity} {...newProps} />} />
  )
};

const styles = () => createStyles({
  container: {
    overflow: 'hidden'
  }
});

const drawerMenu = [
  {
    'title': 'Startseite',
    'key': 'home',
    'items': [
      {
        'title': 'Übersicht',
        'key': 'overview',
        'target': '/',
        'icon': 'home'
      }
    ]
  },
  {
    'title': 'Status und Bedienung',
    'key': 'status',
    'items': [
      {
        'title': 'Geräte',
        'key': 'devices',
        'target': '/devices',
        'icon': undefined
      },
      {
        'title': 'Räume',
        'key': 'rooms',
        'target': '/rooms',
        'icon': undefined
      },
      {
        'title': 'Gewerke',
        'key': 'functions',
        'target': '/functions',
        'icon': undefined
      }
    ]
  },
  {
    'title': 'Programme und Verknüpfungen',
    'key': 'programs_links',
    'items': [
      {
        'title': 'Direktverknüpfungen',
        'key': 'directLink',
        'target': '/directlinks',
        'icon': 'link'
      },
      {
        'title': 'Programme',
        'key': 'programs',
        'target': '/programs',
        'icon': 'code'
      }
    ]
  }
];

const actionMenu = [
  {
    'title': 'Alarmmeldungen',
    'key': 'alarms',
    'target': '/alarms',
    'icon': 'warning',
    'badge': 5
  },
  {
    'title': 'Servicemeldungen',
    'key': 'notifications',
    'target': '/notifications',
    'icon': 'notifications'
  },
  {
    'title': 'Anlernen',
    'key': 'teach',
    'target': '/teach',
    'icon': 'add_circle',
    'truncate' : true
  },
  {
    'title': 'Abmelden',
    'key': 'logout',
    'target': '/logout',
    'icon': 'account_circle',
    'truncate' : true
  }
]

export namespace Container {
  export interface Props {
  }

  export type AllProps = Props & WithStyles<typeof styles> & RouteComponentProps
}

export const Container = hot(module)(withStyles(styles)(withRouter(
class Container extends React.Component<Container.AllProps> {
  componentDidMount() {
    document.title = this.props.location.pathname

    const { history, location } = this.props

    if (location.pathname === '/addons/homer/') {
      history.push('/')
    }
  }

  componentDidUpdate() {
    document.title = this.props.location.pathname
  }

  render() {
    const { classes, location, match } = this.props;

    console.log(location.pathname + " " + location.search + " " + match.path + " " + match.isExact + " " + match.url)

    let dMenu = getPathDepth(match.path) >= 2 ? undefined : drawerMenu;
    let aMenu = dMenu === undefined ? undefined : actionMenu;

    return (
      <div className={classes.container}>
        <Header title={match.path} drawerMenu={dMenu} actionMenu={aMenu} />
        <TransitionGroup>
          <Slide direction='up' key={match.path}>
            <Switch>
              <ActivityRoute exact path="/" activity={App} />
              <ActivityRoute path="/functions" activity={Functions} />
              <ActivityRoute path="/rooms" activity={App} />
              <ActivityRoute path="/devices" activity={Devices} />
              <ActivityRoute activity={Empty} />
            </Switch>
          </Slide>
        </TransitionGroup>
      </div>
    )
  }
}
)))