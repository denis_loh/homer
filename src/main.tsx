import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { configureStore } from 'app/store';
import { Router } from 'react-router';
import { Container } from './app';
import { createBrowserHistory } from 'history';

// prepare store
const store = configureStore()
const history = createBrowserHistory({ basename:'/addon/homer' })

ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <Container />
    </Router>
  </Provider>,
  document.getElementById('root')
);
